#include <stdio.h>
typedef struct{
    int TopLeftX;
    int TopLeftY;
    int BottomRightX;
    int BottomRightY;
} RECT;

int intersection(RECT *prt1, RECT *prt2)
{
    if(prt2->BottomRightY < prt1->TopLeftY || prt2->TopLeftY > prt1->BottomRightY
        || prt2->TopLeftX > prt1->BottomRightX || prt2->BottomRightX < prt1->TopLeftX)
    {
        printf("Do not intersect\n");
        return 0;
    }
    else
    {
        if(prt2->TopLeftX >= prt1->TopLeftX && prt2->TopLeftX <= prt1->BottomRightX)
        {
            if(prt2->BottomRightX > prt1->BottomRightX)
            {
                if(prt2->TopLeftY < prt1->TopLeftY)
                {
                    if(prt2->BottomRightY <= prt1->BottomRightY)
                    {
                        printf("%d %d %d %d\n",prt2->TopLeftX, prt1->TopLeftY, prt1->BottomRightX, prt2->BottomRightY);
                    }
                    else
                    {
                        printf("%d %d %d %d\n",prt2->TopLeftX, prt1->TopLeftY, prt1->BottomRightX, prt1->BottomRightY);
                    }
                }
                else if(prt2->TopLeftY >= prt1->TopLeftY && prt2->TopLeftY <= prt1->BottomRightY)
                {
                    if(prt2->BottomRightY <= prt1->BottomRightY)
                    {
                        printf("%d %d %d %d\n",prt2->TopLeftX, prt2->TopLeftY, prt1->BottomRightX, prt2->BottomRightY);
                    }
                    else
                    {
                        printf("%d %d %d %d\n",prt2->TopLeftX, prt2->TopLeftY, prt1->BottomRightX, prt1->BottomRightY);
                    }
                }
            }
            else if(prt2->BottomRightX >= prt1->TopLeftX && prt2->BottomRightX <= prt1->BottomRightX)
            {
                if(prt2->TopLeftY < prt1->TopLeftY)
                {
                    if(prt2->BottomRightY < prt1->BottomRightY)
                    {
                        printf("%d %d %d %d\n",prt2->TopLeftX, prt1->TopLeftY, prt2->BottomRightX, prt2->BottomRightY);
                    }
                    else
                    {
                        printf("%d %d %d %d\n",prt2->TopLeftX, prt1->TopLeftY, prt2->BottomRightX, prt1->BottomRightY);
                    }
                }
            }
        }
        else if(prt2->TopLeftX < prt1->TopLeftX)
        {
            if(prt2->TopLeftY < prt1->TopLeftY)
            {
                if(prt2->BottomRightX >= prt1->TopLeftX && prt2->BottomRightX <= prt1->BottomRightX)
                {
                    if(prt2->BottomRightY >= prt1->TopLeftY && prt2->BottomRightY <= prt1->BottomRightY)
                    {
                        printf("%d %d %d %d\n",prt1->TopLeftX, prt1->TopLeftY, prt2->BottomRightX, prt2->BottomRightY);
                    }
                    else if(prt2->BottomRightY > prt1->BottomRightY)
                    {
                        printf("%d %d %d %d\n",prt1->TopLeftX, prt1->TopLeftY, prt2->BottomRightX, prt1->BottomRightY);
                    }
                }
                else if(prt2->BottomRightX > prt1->BottomRightX)
                {
                    if(prt2->BottomRightY >= prt1->TopLeftY && prt2->BottomRightY <= prt1->BottomRightY)
                    {
                        printf("%d %d %d %d\n",prt1->TopLeftX, prt1->TopLeftY, prt1->BottomRightX, prt2->BottomRightY);
                    }
                }
            }
            else if(prt2->TopLeftY >= prt1->TopLeftY && prt2->TopLeftY <= prt1->BottomRightY)
            {
                if(prt2->BottomRightX >= prt1->TopLeftX && prt2->BottomRightX <= prt1->BottomRightX)
                {
                    if(prt2->BottomRightY >= prt1->TopLeftY && prt2->BottomRightY <= prt1->BottomRightY)
                    {
                        printf("%d %d %d %d\n",prt1->TopLeftX, prt2->TopLeftY, prt2->BottomRightX, prt2->BottomRightY);
                    }
                    else if(prt2->BottomRightY > prt1->BottomRightY)
                    {
                        printf("%d %d %d %d\n",prt1->TopLeftX, prt2->TopLeftY, prt2->BottomRightX, prt1->BottomRightY);
                    }
                }
                else if(prt2->BottomRightX > prt1->BottomRightX)
                {
                    if(prt2->BottomRightY >= prt1->TopLeftY && prt2->BottomRightY <= prt1->BottomRightY)
                    {
                        printf("%d %d %d %d\n",prt1->TopLeftX, prt2->TopLeftY, prt1->BottomRightX, prt2->BottomRightY);
                    }
                    else if(prt2->BottomRightY > prt1->BottomRightY)
                    {
                        printf("%d %d %d %d\n",prt1->TopLeftX, prt2->TopLeftY, prt1->BottomRightX, prt1->BottomRightY);
                    }
                }
            }
        }
        return 0;
    }
}
int main()
{
    RECT prt1,prt2;
    int x;
    printf("enter top left coordinates of 1st rectangle\n");
    scanf("%d %d",&prt1.TopLeftX,&prt1.TopLeftY);
    printf("enter bottom right coordinates of 1st rectangle\n");
    scanf("%d %d",&prt1.BottomRightX,&prt1.BottomRightY);
    printf("enter top left coordinates of 2nd rectangle\n");
    scanf("%d %d",&prt2.TopLeftX,&prt2.TopLeftY);
    printf("enter bottom right coordinates of 2nd rectangle\n");
    scanf("%d %d",&prt2.BottomRightX,&prt2.BottomRightY);
    intersection(&prt1,&prt2);
    return 0;
}

