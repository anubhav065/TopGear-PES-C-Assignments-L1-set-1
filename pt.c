/*
Design:
1. enter the height of tree to be printed.
2. based on the number of row from top to bottom, calculate binomial co-efficients recursively.
3. print the binomial co-efficients in each row.
*/
#include <stdio.h>
typedef long long int lli;
lli fac(int n)
{
    if(n==1||n==0)
    {
        return 1;
    }
    else
    {
        return n*fac(n-1);
    }
}
int main()
{
    int n,i, x=0;
    lli a,b,c;
    printf("enter the height of tree\n");
    scanf("%d",&n);
    n=n-1;
    while(n>=0)
    {
        i=0;
        a = fac(n);
        while(i<=n)
        {
            b = fac(i);
            c = fac(n-i);
            printf("%lld ",a/(b*c));
            i++;
        }
        x++;
        n--;
        printf("\n");
        for(i=0;i<x;i++)
        {
            printf(" ");
        }
    }
    return 0;
}