/*
Design:
1. Read each word from the file till a newline/EOF is encountered.
2. Convert the word into decimal notation
3. Calculate the number of bits set to 1 in the number.
4. If the number of bits set to 1 is greater than previous numbers, it is saved.
5. Of all the numbers saved, the numbers which have number of bits set to the highest encountered value are printed.
*/

//NOTE: Please store the list of numbers in a file named "numbers.txt", and save it in the same folder where this code is saved.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
int get1Count(int dec_n)
{
    int count=0;
    while(dec_n>=1)
    {
        if(dec_n%2)
        {
            count++;
        }
        dec_n/=2;
    }
    return count;
}
int main()
{
    FILE *fp;
    int i,
        j=0, //index to keep track of numbers which have more number of bits set to 1 compared to previous ones.
        count_max[1000], //array of higher number of bits set to 1 compared to previous ones.
        max=0, //highest number of bits encountered in a number
        n, //store each char letter as an integer.
        exp_, //to multiply 'n' with 10 to the power the position 'n' is at.
        dec_n; //each word in the txt file converted to decimal.

    char ch,
        val[1000],//store a word from current row.
        cpy_val[1000][1000];//array of numbers which have more number of bits set to 1 compared to previous ones.
        
    fp = fopen("numbers.txt","r");
    if(fp==NULL)
    {
        printf("Cannot open file\n");
        exit(1);
    }
    ch = fgetc(fp);
    while(ch!=EOF)
    {
    	i=0;
    	n=0;
    	exp_=0;
    	dec_n=0;
    	while(ch!='\n')
    	{
    		if(ch==EOF) //last entry in txt file
    		{
    			break;
    		}
    		else
    		{
    			val[i] = ch;
    			i++;
    			ch = fgetc(fp);
    		}
    	}
    	val[i] = '\0';

    	//convert hexadecimal to decimal
    	if(val[1]=='x'||val[1]=='X')
    	{
    	    for(i=strlen(val)-1;i>=2;i--)
    	    {
    	        if( val[i]>=65 && val[i]<=70 )
    	        {
    	            n = val[i]-'7'; //convert A,B,C,D,E,F to integer decimal
    	            dec_n = dec_n + n*pow(16,exp_);
    	            exp_++;
    	        }
            	else if( val[i]>=97 && val[i]<=102 )
            	{
                	n = val[i] - 'W'; //convert a,b,c,d,e,f to integer decimal
                	dec_n = dec_n + n*pow(16,exp_);
                	exp_++;
            	}
            	else if( val[i]>=49 && val[i]<=57)
            	{
                	n = val[i] - '0';
                	dec_n = dec_n + n*pow(16,exp_);
                	exp_++;
            	}
    	    }
    	}

    	//convert octal to decimal
    	else if(val[0]=='0')
    	{
    		for(i=strlen(val)-1;i>=1;i--)
        	{
        	    if( val[i]>=49 && val[i]<=56 ) //octal valbers have digits 0 to 8
        	    {
        	        n = val[i] - '0';
        	        dec_n = dec_n + n*pow(8,exp_);
        	        exp_++;
        	    }
        	}
    	}

    	//get decimal from txt file
    	else
    	{
    		for(i=0;i<strlen(val);i++)
        	{
        	    dec_n = dec_n*10 + (val[i]-'0');
        	}
    	}
    	if(get1Count(dec_n)>=max)
    	{
    		max = get1Count(dec_n);
    		count_max[j] = get1Count(dec_n);
    		strcpy(cpy_val[j], val);
    		j++;
    	}
    	memset(val,0,sizeof(val));
    	if(ch!=EOF)//to stop unnecessarily reading file when last entry is read.
    	{
    		ch=fgetc(fp); //ch was already set to EOF in the second while loop for last entry, so here it should point to element next to EOF, and so loop should be infinite, but that's not the case
    	}
    }
    for(i=0;i<j;i++)
    {
    	if(max==count_max[i])
    	{
    		printf("%s - %d\n",cpy_val[i],count_max[i]);
    	}
    }
    return 0;
}