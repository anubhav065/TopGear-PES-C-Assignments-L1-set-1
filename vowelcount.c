/*
Design:
1. Read each character from the file till a space/EOF is encountered so a word can be checked for vowels.
2. Count the number of vowels in the word.
3. If the number of vowels is higher than the previous maximum value, then store the word in a 2d char array.
4. Out of those words in the 2d array, the word which has number of vowels equal to the highest encountered value is printed.
*/

//Keep the sample.txt file in the same folder in which this code is saved.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main()
{
	FILE *fp;
	char ch,
		cpy_word[100], //used to store the current word being evaluated
		max_word[][1000]={0};//to keep track of all the words which had higher number of vowels than the previous one.
	int ctr,
		max=0, //keep track of the highest number of vowels in a word
		j=0, //pointer for cpy_word.
		k=0, //pointer for max_word.
		max_vowels[1000];
	fp = fopen("sample.txt","r"); //keep the .txt file in the same folder where this code is saved.
	if(fp==NULL)
	{
		printf("cannot open file");
		exit(1);
	}
	ch=fgetc(fp);
	while(ch!=EOF)
	{
		if( (ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122) )
		{
			ctr=0;
			j=0;
			while(ch!=' ')
			{
				if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U')
				{
					ctr++;
				}
				else if(ch == '.' || ch == ',' || ch == '"' || ch == '(' || ch == ')')//ignore period, commas, quotes and brackets
				{
					ch = fgetc(fp);
					continue;
				}
				else if(ch == EOF)
				{
					break;
				}
				cpy_word[j] = ch;
				ch = fgetc(fp);
				j++;
			}
			if(max<=ctr)
			{
				max=ctr;
                max_vowels[k]=ctr;
                strcpy(max_word[k],cpy_word);
                k++;
			}
		}
		ch =fgetc(fp);
		memset(cpy_word,0,sizeof(cpy_word)); //clear the current word array so new word can be saved and copied to max_word.
	}
    for(j=0;j<k;j++)
    {
        if(max==max_vowels[j])
        {
            printf("%d %s\n",max,max_word[j]);//the length of word stored is an issue     
        }
    }
	fclose(fp);
	return 0;
}
